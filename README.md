# README #

Python code for creating radio weak lensing history (and future) timeline plots.

Requires:

- Python
- numpy
- matplotlib

wl_surveys.txt contains details of each survey. Use 'o' for optical and 'r' for radio.

wl_surveys.py makes the following plots:

- Area vs. n_gal
- Year vs. n_gal
- Year vs. Total_gal