import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
import sys

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

if sys.version_info[0] < 3 :
  data = np.genfromtxt('wl_surveys_2020.txt', dtype='string')
else:
  data = np.genfromtxt('wl_surveys_2020.txt', dtype=str)

f1 = plt.figure(1, figsize=(4.5, 3.75))
f2 = plt.figure(2, figsize=(4.5, 3.75))
f3 = plt.figure(3, figsize=(4.5, 3.75))
delta = 0.1

data[:,4][data[:,4]=='o'] = 'b'
data[:,4][data[:,4]=='g'] = 'b'

for sur in data:

  area = float(sur[1])
  ngal = float(sur[2])
  year = float(sur[5])
  total_gal = area*ngal*60*60
  
  #
  # Plot of area vs ngal
  #
  f1.gca().loglog(area, ngal, 'o', markerfacecolor=sur[4], markeredgecolor=sur[4], markersize=4)
  # Some twiddling for indivudal plot labels
  if sur[0] == 'LSST' or sur[0] == 'CFHTLens':
    f1.gca().text(area*(1+delta), ngal*(1-2*delta), sur[0], fontsize=8)
  elif sur[0] == 'FIRST':
    f1.gca().text(area*(1+3*delta), 1.3*(1+delta), 'FIRST\n({0})'.format(ngal), fontsize=8)
    f1.gca().plot(area*(1+delta), 1.5, 'v', markerfacecolor=sur[4], markeredgecolor=sur[4], markersize=4)
  else:
    f1.gca().text(area*(1+delta), ngal*(1+delta), sur[0], fontsize=8)

  #
  # Plot of year vs total_gal
  #
  # Some by-eye line fits
  x_optical = [2000,2033]
  y_optical = [1.e5, 8e9]
  #x_radio = [2008,2038]
  x_radio = [2015,2042]
  y_radio = [7.e1, 8e9]
  f2.gca().plot(x_optical, y_optical, 'b--', color='powderblue', lw=1, zorder=-1)
  f2.gca().plot(x_radio, y_radio, '--', color='lightcoral', lw=1, zorder=-1)
  f2.gca().semilogy(year, total_gal, 'o', markerfacecolor=sur[4], markeredgecolor=sur[4], markersize=4)
  # Some twiddling for indivudal plot labels
  if sur[0] == 'COMBO-17':
    f2.gca().text(year*(1+0.002*delta), total_gal*(1-4*delta), sur[0], fontsize=8)
  elif sur[0] == 'HST-COSMOS':
    f2.gca().text(year*(1+0.002*delta), total_gal*(1-4*delta), sur[0], fontsize=8)
  else:
    f2.gca().text(year*(1+0.002*delta), total_gal*(1+delta), sur[0], fontsize=8)

  #
  # Plot of year vs n_gal
  #
  f3.gca().semilogy(year, ngal, 'o', markerfacecolor=sur[4], markeredgecolor=sur[4], markersize=4)
  # Some twiddling for indivudal plot labels
  if sur[0] == 'COMBO-17':
    f3.gca().text(year*(1+0.002*delta), ngal*(1-4*delta), sur[0], fontsize=8)
  else:
    f3.gca().text(year*(1+0.002*delta), ngal*(1+delta), sur[0], fontsize=8)

f1.gca().set_xlabel('Area [deg$^2$]', fontsize='x-large')
f1.gca().set_ylabel('Galaxy Number Density [arcmin$^{-2}$]', fontsize='x-large')
f1.gca().set_ylim([1,50])
f1.gca().set_xlim([1.e-2,3e5])

f2.gca().set_xlabel('Year', fontsize='x-large')
f2.gca().set_ylabel('Total Galaxies', fontsize='x-large')
f2.gca().grid(linestyle='--', alpha=0.6)
f2.gca().set_xlim([2000,2045])
f2.gca().set_ylim([8.e1,8.e9])

f3.gca().set_xlabel('Year', fontsize='x-large')
f3.gca().set_ylabel('Galaxy Number Density [arcmin$^{-2}$]', fontsize='x-large')
f3.gca().set_xlim([2000,2045])
f3.gca().set_ylim([1,50])

f1.savefig('wl-surveys_area-ngal_2020.png', bbox_inches='tight', dpi=300)
f2.savefig('wl-surveys_year-totgal_2020.png', bbox_inches='tight', dpi=300)
f3.savefig('wl-surveys_year-ngal_2020.png', bbox_inches='tight', dpi=300)
